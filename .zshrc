##---
# zsh config
##---


## History file
HISTFILE=~/.zsh_historyfile
HISTSIZE=10000
SAVEHIST=10000


## Run TMUX
if [ "$TMUX" = "" ]; then tmux; fi


############ STYLING ################

setprompt() {
  setopt prompt_subst

  if [[ -n "$SSH_CLIENT"  ||  -n "$SSH2_CLIENT" ]]; then
    p_host='%F{yellow}%M%f'
  else
    p_host='%F{green}%M%f'
  fi

  PS1=${(j::Q)${(Z:Cn:):-$'
    %F{cyan}[%f
    %(!.%F{red}%n%f.%F{yellow}%n%f)
    %F{cyan}%f
    #%F{cyan}@%f
    #${p_host}
    %F{cyan}][%f
    %F{cyan}%~%f
    %F{cyan}]%f
    %(!.%F{red}%#%f.%F{green}%#%f)
    " "
  '}}

  PS2=$'%_>'
  RPROMPT=$'${vcs_info_msg_0_}'
}
setprompt


## Colored Manpages
man() {
  env \
    LESS_TERMCAP_mb=$(printf "\e[1;31m") \
    LESS_TERMCAP_md=$(printf "\e[1;31m") \
    LESS_TERMCAP_me=$(printf "\e[0m") \
    LESS_TERMCAP_se=$(printf "\e[0m") \
    LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
    LESS_TERMCAP_ue=$(printf "\e[0m") \
    LESS_TERMCAP_us=$(printf "\e[1;32m") \
    man "$@"
}

## Sytax higlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

## Dir colorize
LS_COLORS='rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34>'
export LS_COLORS

## Additional
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#ff00ff,bg=cyan,bold,underline"

# Color stderr
#exec 2>>(while read line; do
#  print '\e[91m'${(q)line}'\e[0m' > /dev/tty; print -n $'\0'; done &)


########### FUNCTION ###############

## Dir navigation
# setopt  autocd autopushd 

## fzf
#source /usr/share/fzf/key-bindings.zsh
#source /usr/share/fzf/completion.zsh

## autocomplete
#source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

## completion
#source /usr/share/zsh/functions/Zle/cycle-completion-positions

## Color TAB choose

## Setops
# dont beep by error
#setopt no_beep
# Don't kill background jobs when I logout
#setopt nohup
# completion
#setopt automenu
#setopt cdablevars

## Bindkeys
# use strg+<-|->
#bindkey "^[[1;5C" forward-word
#bindkey "^[[1;5D" backward-word
# search history
#bindkey '\e[A' history-search-backward
#bindkey '\e[B' history-search-forward
bindkey '\e[A' history-beginning-search-backward
bindkey '\e[B' history-beginning-search-forward
# use standard keysettings
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[5~" beginning-of-history
bindkey "\e[6~" end-of-history
bindkey "\e[7~" beginning-of-line
bindkey "\e[3~" delete-char
bindkey "\e[2~" quoted-insert
bindkey "\e[5C" forward-word
bindkey "\e[5D" backward-word
bindkey "\e\e[C" forward-word
bindkey "\e\e[D" backward-word
bindkey "\e[1;5C" forward-word
bindkey "\e[1;5D" backward-word
bindkey "\e[8~" end-of-line
bindkey "\eOH" beginning-of-line
bindkey "\eOF" end-of-line
bindkey "\e[H" beginning-of-line
bindkey "\e[F" end-of-line

## Compinit
#zmodload zsh/complist
#autoload -Uz compinit
autoload -U compinit
compinit

## Zstyle
#zstyle :compinstall filename '${HOME}/.zshrc'
zstyle ':completion:*' menu select matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
#zstyle ':completion:*' menu select
# reload $PATH
#zstyle ':completion:*' rehash true
# show programm preview per letter
#autoload predict-on
#predict-on

## Exports
# $PATH
export PATH=/usr/local/bin:/usr/sbin:/sbin:/usr/bin:/bin:/home/cc1p/.python:
# Flatpak hacks
#export QT_QPA_PLATFORMTHEME=gtk2 #qt5ct
#export QT_STYLE_OVERRIDE=gtk2
#export QT_AUTO_SCREEN_SCALE_FACTOR=0 
export GTK_THEME=Arc-Dark
#export QT_SCALE_FACTOR=1
#export QT_QPA_PLATFORMTHEME=qt5ct
#export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
# EDITOR
export EDITOR=/usr/bin/nano


################### ALIAS ######################

## Alias
# ls
alias l="ls --color -lha"
# nano
alias nano="nano -c"
# date
alias date="echo "____________________________" && date && echo "____________________________" && cal && echo "____________________________"
"
# cd
alias ..="cd ../"
# mkdir
alias mkdir="mkdir -p"
# cp
alias cp="rsync -avP"
# dp0|1
alias dp0="xrandr --output DP-2-1 --off --output eDP-1 --auto"
alias dp1="xrandr --output eDP-1 --off --output DP-2-1 --auto"
# git
alias gu="git add . && git commit -m "lazy_update" && git push origin master"
alias gc="git clone"
# fzf
alias p="pacman -Slq | fzf -m --preview 'pacman -Si {1}' | xargs -ro sudo pacman -S"
